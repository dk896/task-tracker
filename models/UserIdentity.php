<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use app\models\user\User;
use app\models\user\UserQuery;

class UserIdentity extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;

    public function __construct($id, $username, $password)
    {
        parent::__construct();

        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Finds user by ID
     *
     * @param string $id
     *
     * @return UserIdentity|null
     */
    public static function findIdentity($id)
    {
        $userRecord = User::findOne($id);

        $userIdentity = new self(
            $userRecord->id,
            $userRecord->login,
            $userRecord->password
        );

        return $userIdentity;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return UserIdentity|null
     */
    public static function findByUsername($username)
    {
        $userIdentity = null;
        $userRecord = (new UserQuery(User::className()))
            ->findByUsername($username)
            ->one();

        if ($userRecord !== null) {
            $userIdentity = new self(
                $userRecord->id,
                $userRecord->login,
                $userRecord->password
            );
        }

        return $userIdentity;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Find Identity By Access Token
     * Useful in case of authorization like OAuth2 or OpenID

     * @param string $token - Token for looking.
     * @param mixed  $type  - Type of token, depends on the implementation.
     *
     * @return void
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // stub
        throw new NotSupportedException(
            'You can only login by username/password pair for now'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
    }

    /**
     * Validate given auth key.
     *
     * @param string $authKey - the given auth key
     *
     * @return boolean
     */
    public function validateAuthKey($authKey)
    {
    }

    /**
     * Validate given password.
     *
     * @param string $plainString - the given password
     * @param string $hash        - verify against password
     *
     * @return boolean whether the password is correct.
     */
    public function validatePassword($plainString, $hash)
    {
        return Yii::$app->security->validatePassword($plainString, $hash);
    }
}
