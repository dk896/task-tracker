<?php

namespace app\models\task;

use app\models\task\Task;
use yii\db\ActiveQuery;

class TaskQuery extends ActiveQuery
{
    public $query;

    public function init()
    {
        parent::init();
        $this->query = Task::find();
    }
}
