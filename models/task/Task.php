<?php

namespace app\models\task;

use app\common\events\MailsDataEvent;
use app\models\tasks_comment\TasksComment;
use app\models\tasks_image\TasksImage;
use app\models\user\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "task".
 *
 * @property int    $id
 * @property string $name
 * @property string $date
 * @property string $description
 * @property int    $user_id
 *
 * @property User $user
 */
class Task extends \yii\db\ActiveRecord
{
    const EVENT_TASK_CREATED = 'new_task_created';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'deadline'], 'required'],
            [['deadline'], 'safe'],
            [['description'], 'string'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Task title',
            'deadline' => 'Deadline',
            'description' => 'Description',
            'user_id' => 'User`s ID',
            'done' => 'Have been done or not?',
        ];
    }

    /**
     * Describe relation Task -- User
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Describe relation Task -- TasksComment
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasMany(TasksComment::className(), ['task_id' => 'id']);
    }

    /**
     * Describe relation Task -- TasksImage
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasMany(TasksImage::className(), ['task_id' => 'id']);
    }

    /**
     * Get Active Record for user and current month
     * @param  integer $userId User's ID
     *
     * @return array Of Task Active Record
     */
    public static function getByCurrentMonth($userId)
    {
        return static::find()
            ->where(['user_id' => $userId])
            ->andWhere(['MONTH([[deadline]])' => date('n')])
            ->all();
    }

    public function afterSave($insert, $changedAttributes)
    {
        $return = parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $event = new MailsDataEvent();
            $event->email = $this->user->email;
            $event->subject = 'New Task ID#' . $this->id;
            $event->body
            = 'There was created new Task ID# '
            . $this->id . '<br>'
            . $this->description . '<br>'
            . 'Link '
            . '@web/admin-task/view?id='
            . $this->id . '<br>'
            . 'Deadline: ' . $this->deadline;
            $event->fromName = 'Admin: creight_glanz';
            $event->fromAddress = 'cglanz4@google.co.uk';
            $event->sender = $this;

            $this->trigger(static::EVENT_TASK_CREATED, $event);
        }

        return $return;
    }
}
