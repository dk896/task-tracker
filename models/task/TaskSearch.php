<?php

namespace app\models\task;

use app\models\task\Task;
use app\models\task\TaskQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TaskSearch represents the model behind the search form of `app\models\task\Task`.
 */
class TaskSearch extends Task
{
    /**
     * @var string date for search from
     */
    public $deadlineFrom;

    /**
     * @var string date for search from
     */
    public $deadlineTo;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [
                [
                    'name',
                    'deadline',
                    'description',
                    'deadlineFrom',
                    'deadlineTo',
                    'user.first_name',
                    'user.last_name'
                ],
                'safe'
            ],
        ];
    }

    public function attributes()
    {
        // делаем поле зависимости доступным для поиска
        return array_merge(parent::attributes(), ['user.first_name', 'user.last_name']);
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = Task::find(); // SELECT * FROM task
        $query = (new TaskQuery(Task::class))->query;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want
            // to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([ // user_id = :user_id
            'id' => $this->id,
            'deadline' => $this->deadline,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['>=', 'deadlineFrom', $this->deadlineFrom])
            ->andFilterWhere(['<=', 'deadlineTo', $this->deadlineTo])
            ;

        return $dataProvider;
    }
}
