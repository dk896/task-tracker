<?php

namespace app\models\tasks_image;

use app\models\task\Task;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "tasks_image".
 *
 * @property int    $id
 * @property string $name
 * @property int    $task_id
 * @property Task   $task
 */
class TasksImage extends ActiveRecord
{
    const PATH_TO_IMG = '@webroot/img/';
    const PATH_TO_SMALL_IMG = '@webroot/img/small/';

    /**
     * {@inheritDoc}
     */
    public static function tableName()
    {
        return 'tasks_image';
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [
                ['task_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Task::class,
                'targetAttribute' => ['task_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Image ID',
            'name' => 'Task`s image name',
            'task_id' => 'Task`s ID',
        ];
    }

    /**
     * Describe relation TasksImage -- Task
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }
}
