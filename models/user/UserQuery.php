<?php


namespace app\models\user;

use app\models\user\User;
use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{
    public $query;

    public function init()
    {
        parent::init();
        $this->query = User::find();
    }

    public function findByUsername($username)
    {
        return $this->query->where('[[login]]=:username', [':username' => $username]);
    }

    /**
     * {@inheritdoc}
     *
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     *
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
