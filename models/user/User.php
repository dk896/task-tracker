<?php

namespace app\models\user;

use app\models\role\Role;
use Yii;
use yii\behaviors\AttributeTypecastBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property integer $role_id
 * @property Role $role
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'typecast' => [
                'class' => AttributeTypecastBehavior::className(),
                'attributeTypes' => [
                    'role_id' => AttributeTypecastBehavior::TYPE_INTEGER,
                ],
                'typecastAfterValidate' => true,
                'typecastBeforeSave' => false,
                'typecastAfterFind' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password', 'email'], 'required'],
            [['login'], 'string', 'max' => 50],
            [['first_name'], 'string', 'max' => 50],
            [['last_name'], 'string', 'max' => 50],
            [['email'], 'email'],
            [['password'], 'string', 'max' => 100],
            ['role_id', 'integer'],
            [
                ['role_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Role::className(),
                'targetAttribute' => ['role_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
        ];
    }

    /**
     * Whether method call before insert
     *
     * @param boolean $insert Status
     *
     * @return boolean
     */
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password') || $this->isNewRecord) {
            $this->password = Yii::$app->security->generatePasswordHash(
                $this->password
            );
        }

        return $return;
    }

    public function getRole()
    {
        return $this->hasOne(Role::class, ['id' => 'role_id']);
    }

    public static function getAdmins()
    {
        return static::find()->where(['role_id' => 1])->all();
    }
}
