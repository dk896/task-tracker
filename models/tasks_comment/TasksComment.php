<?php

namespace app\models\tasks_comment;

use app\models\task\Task;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "tasks_comment".
 *
 * @property int    $id
 * @property string $body
 * @property int    $task_id
 * @property Task   $task
 */
class TasksComment extends ActiveRecord
{
    /**
     * {@inheritDoc}
     */
    public static function tableName()
    {
        return 'tasks_comment';
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['body'], 'required'],
            [['body'], 'string', 'max' => 255],
            [['task_id'], 'integer'],
            [
                ['task_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Task::class,
                'targetAttribute' => ['task_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Comment ID',
            'body' => 'Task`s comment',
            'task_id' => 'Task`s ID',
        ];
    }

    /**
     * Describe relation TasksComment -- Task
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }
}
