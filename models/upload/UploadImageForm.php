<?php

namespace app\models\upload;

use app\models\tasks_image\TasksImage;
use Yii;
use yii\base\Model;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * UploadeImage class used for uploading images
 */
class UploadImageForm extends Model
{
    /**
     *
     * @var UploadedFile
     */
    public $imageFile;
    public $newImageName;

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            [
                ['imageFile'],
                'image',
                'skipOnEmpty' => false,
                'extensions' => 'png, jpg, jpeg, gif, pjpeg, webp',
                'mimeTypes' => ['image/*'],
                'maxSize' => 1024 * 1024,
            ],
        ];
    }

    /**
     * Function for upload image file
     *
     * @return boolean
     */
    public function upload()
    {
        if ($this->validate()) {
            $length = 13;
            $bytes = \random_bytes(ceil($length / 2));
            $imageNewName = \substr(bin2hex($bytes), 0, $length);
            $this->newImageName = $imageNewName;

            $pathBase = Yii::getAlias(
                TasksImage::PATH_TO_IMG
                . $this->newImageName
                . '.'
                . $this->imageFile->extension
            );

            $pathSmall = Yii::getAlias(
                TasksImage::PATH_TO_SMALL_IMG
                . $this->newImageName
                . '.'
                . $this->imageFile->extension
            );

            $this->imageFile->saveAs($pathBase);

            $width = 60;
            $height = 60;
            $this->resizeImage($pathBase, $pathSmall, $width, $height);

            return true;
        } else {
            return false;
        }
    }

    /**
     * Resize image and save to new path
     *
     * @param string  $srcPath    Path to src
     * @param string  $targetPath Target path for save
     * @param integer $width      Thumbernail`s width
     * @param integer $height     Thumbernail`s height
     *
     * @return boolean
     */
    public function resizeImage($srcPath, $targetPath, $width, $height)
    {
        Image::thumbnail($srcPath, $width, $height)
            ->save($targetPath);

        return true;
    }

}
