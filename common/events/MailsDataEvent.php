<?php

namespace app\common\events;

use Yii;
use yii\base\Event;

class MailsDataEvent extends Event
{
    public $fromName;
    public $fromAddress;
    public $email;
    public $subject;
    public $body;
}
