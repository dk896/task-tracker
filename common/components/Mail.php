<?php

namespace app\common\components;

use Yii;
use yii\base\Component;
use app\common\events\MailsDataEvent;

class Mail extends Component
{

    public function sendMailHandler(MailsDataEvent $event)
    {
        Yii::$app->mailer->compose()
            ->setTo($event->email)
            ->setFrom([
                $event->fromAddress => $event->fromName
            ])
            ->setSubject($event->subject)
            ->setTextBody($event->body)
            ->send();

        return true;
    }
}
