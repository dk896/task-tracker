<?php
/**
 *
 */

namespace app\commands;

use app\models\task\Task;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Send reminding emails based on DB data about deadlines.
 */
class ReminderController extends Controller
{
    /**
     * Desplay info about reminder.
     *
     * @param string $message the message to be echoed.
     *
     * @return int Exit code
     */
    public function actionIndex($message = 'I am your Reminder)')
    {
        echo $message . "\n";
        echo 'Usage: ' . "\n";
        echo 'remider/get true
        # Get amount of memembers to remind and show 1st' . "\n";
        echo 'remider/send # Send messages on email' . "\n";

        return ExitCode::OK;
    }

    /**
     * Get members for reminding about `Deadline`
     *  array filled users data:
     * `user full name`, `email`, `deadline`,
     * `interval in days`
     *
     * @return array
     */
    public function actionGet($show = false)
    {
        $checkDate = $this->getCheckDate();
        $currentDate = \date('Y-m-d H:i:s');

        $taskQuery = Task::find();
        $tasksCount = $taskQuery
            ->where(['done' => 0])
            ->andWhere(['>=', 'deadline', $currentDate])
            ->andWhere(['<=', 'deadline', $checkDate])
            ->count();

        if ($tasksCount) {
            $membersRecords = $taskQuery
                ->where(['done' => 0])
                ->andWhere(['>=', 'deadline', $currentDate])
                ->andWhere(['<=', 'deadline', $checkDate])
                ->all();

            $membersToRemind = [];

            Console::startProgress(0, $tasksCount);
            foreach ($membersRecords as $i => $member) {
                $membersToRemind[$i]['taskName'] = $member->name;
                $membersToRemind[$i]['userFullName']
                = $member->user->first_name . ' ' . $member->user->last_name;
                $membersToRemind[$i]['userEmail'] = $member->user->email;
                $membersToRemind[$i]['deadline'] = $member->deadline;
                $membersToRemind[$i]['daysInterval']
                = $this->getTimeIntervalDays($currentDate, $member->deadline);

                Console::updateProgress($i, $tasksCount);
            }
            Console::endProgress();

            if ($show) {
                echo 'Count of all not done tasks with 3 days interval from now: '
                    . $tasksCount
                    . "\n"
                    . 'Task title'
                    . $membersToRemind[0]['taskName']
                    . "\n"
                    . 'First member to remind: '
                    . 'User name: '
                    . $membersToRemind[0]['userFullName']
                    . "\n"
                    . 'User email: '
                    . $membersToRemind[0]['userEmail']
                    . "\n"
                    . 'Deadline: '
                    . $membersToRemind[0]['deadline']
                    . "\n"
                    . 'Days interval: '
                    . $membersToRemind[0]['daysInterval']
                    . "\n"
                ;

                return ExitCode::OK;
            }
            return $membersToRemind;
        }

        echo 'There is not members to remind for' . "\n";

        return ExitCode::NOINPUT;
    }

    /**
     * Sends emails using the information collected by actionGet().
     *
     * @return boolean
     */
    public function actionSend()
    {
        $divelopers = $this->actionGet();
        $divelopersCount = count($divelopers);
        $adminEmail = 'aludovici0@ft.com';
        $subject = 'About deadlines';

        if ($divelopersCount) {
            Console::startProgress(0, $divelopersCount);
            foreach ($divelopers as $idx => $developer) {
                $email = $developer['userEmail'];

                echo $idx . ': sending email to ' . $email . "\n";

                $name = $developer['userFullName'];
                $date = $developer['deadline'];
                $title = $developer['taskName'];
                $interval = $developer['daysInterval'];

                $body = $this->getBody($name, $interval, $date, $title);

                Yii::$app->mailer->compose()
                    ->setTo($email)
                    ->setFrom($adminEmail)
                    ->setSubject($subject)
                    ->setTextBody($body)
                    ->send();

                Console::updateProgress($idx, $divelopersCount);
            }
            Console::endProgress();

            return ExitCode::OK;
        }

        echo "There are nothing to do. Haven't data.\n";

        return ExitCode::NOINPUT;
    }

    /**
     * Get content`s body for mail letter
     *
     * @param string $name     Name
     * @param string $interval Date inverval
     * @param string $date     Deadline date
     *
     * @return string
     */
    protected function getBody($name, $interval, $date, $title)
    {
        return $subject = sprintf(
            'Dear, %s.<br>
            We would be reach in %s days deadlines %s to task title: `%s`.<br>
            It`s time to make last preparations and show your work.<br>
            Administration<br>',
            $name,
            $interval,
            $date,
            $title
        );
    }

    /**
     * Return date 3 day forward from now
     *
     * @return string
     */
    protected function getCheckDate()
    {
        $threeDaysTime = 3 * 24 * 60 * 60;
        $now = \time();
        $checkTime = $now + $threeDaysTime;
        $checkDate = \date('Y-m-d H:i:s', $checkTime);

        return $checkDate;
    }

    /**
     * Get time interval in array
     *
     * @param string $startTime Starting Date time '2018-06-15 12:00:00'
     * @param string $endTime   Ending Date time '2018-06-16 12:00:00'
     *
     * @return array
     */
    protected function getTimeInterval($startTime, $endTime)
    {
        $endTime = ($endTime == '') ? \date('Y-m-d H:i:s') : $endTime;
        $startDate = new \DateTime($startTime);
        $endDate = new \DateTime($endTime);

        $inverval = $startDate->diff($endDate);
        $timeInterval = [];
        $timeInterval['y'] = $inverval->format('%y');
        $timeInterval['m'] = $inverval->format('%m');
        $timeInterval['d'] = $inverval->format('%d');
        $timeInterval['h'] = $inverval->format('%H');

        return $timeInterval;
    }

    /**
     * Get time interval in days
     *
     * @param string $startTime Starting Date time '2018-06-15 12:00:00'
     * @param string $endTime   Ending Date time '2018-06-16 12:00:00'
     *
     * @return integer
     */
    protected function getTimeIntervalDays($startTime, $endTime)
    {
        return (int) $this->getTimeInterval($startTime, $endTime)['d'];
    }

    /**
     * Check is time interval in days form $timeToCheck to $deadline GE 3
     *
     * @param string $timeToCheck Checking Date Time '2018-06-15 12:00:00'
     * @param string $deadline    Ending Date Time '2018-06-16 12:00:00'
     *
     * @return boolean
     */
    protected function isTime($timeToCheck, $deadline)
    {
        return $this->getTimeIntervalDays($timeToCheck, $deadline) <= 3;
    }
}
