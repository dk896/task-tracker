<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

/**
 * Role Based Access Contoller
 */
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $readTask = $auth->createPermission('readTask');
        $readTask->description = 'Read task description';
        $auth->add($readTask);

        $createTask = $auth->createPermission('createTask');
        $createTask->description = 'Create task';
        $auth->add($createTask);

        $updateTask = $auth->createPermission('updateTask');
        $updateTask->description = 'Update task';
        $auth->add($updateTask);

        $deleteTask = $auth->createPermission('deleteTask');
        $deleteTask->description = 'Delete task';
        $auth->add($deleteTask);

        $readUser = $auth->createPermission('readUser');
        $readUser->description = 'Read users data';
        $auth->add($readUser);

        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create user';
        $auth->add($createUser);

        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update user`s data';
        $auth->add($updateUser);

        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete user';
        $auth->add($deleteUser);
        // Administrator
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $readTask);
        $auth->addChild($admin, $createTask);
        $auth->addChild($admin, $updateTask);
        $auth->addChild($admin, $deleteTask);

        $auth->addChild($admin, $readUser);
        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $deleteUser);
        // Boss
        $boss = $auth->createRole('boss');
        $auth->add($boss);
        $auth->addChild($boss, $readTask);
        $auth->addChild($boss, $createTask);
        $auth->addChild($boss, $updateTask);
        $auth->addChild($boss, $deleteTask);

        $auth->addChild($boss, $readUser);
        // Project manager
        $manager = $auth->createRole('manager');
        $auth->add($manager);
        $auth->addChild($manager, $readTask);
        $auth->addChild($manager, $createTask);
        $auth->addChild($manager, $updateTask);
        $auth->addChild($manager, $deleteTask);

        $auth->addChild($manager, $readUser);
        // Team leader
        $tleader = $auth->createRole('tleader');
        $auth->add($tleader);
        $auth->addChild($tleader, $readTask);
        $auth->addChild($tleader, $createTask);
        $auth->addChild($tleader, $updateTask);
        $auth->addChild($tleader, $deleteTask);

        $auth->addChild($tleader, $readUser);
        // User -- member of crew
        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $readTask);
        $auth->addChild($user, $readUser);
        // Assignments
        $auth->assign($admin, 1);
        $auth->assign($boss, 2);
        $auth->assign($manager, 3);
        $auth->assign($tleader, 4);
        $auth->assign($user, 5);
        $auth->assign($user, 6);
        $auth->assign($user, 7);
        $auth->assign($user, 8);
        $auth->assign($user, 9);
        $auth->assign($user, 10);

        return ExitCode::OK;
    }
}
