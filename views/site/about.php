<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = \Yii::t('app', 'About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?=Html::encode($this->title)?></h1>

    <p>
        This is the Task Manager. Used for create and track tasks for developers working in one team.
    </p>

</div>
