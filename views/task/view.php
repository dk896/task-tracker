<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\tables\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][]
    = ['label' => Yii::t('app/task', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?=Html::encode($this->title)?></h1>

    <?=DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        'deadline',
        'description:html',
        'user_id',
    ],
]);?>
</div>

<div class="row">
    <div class="col-lg-8">

        <button class="btn btn-lg btn-primary">

        <?=Html::a(
            Yii::t('app/task', 'Add comment'),
            Url::to(['@web/task/add-comment', 'task_id' => $model->id], true),
            ['class' => 'link-white']
        );?>

        </button>

        <button class="btn btn-lg btn-primary">

        <?=Html::a(
            Yii::t('app/task', 'Upload image'),
            Url::to(['@web/task/upload-image', 'task_id' => $model->id], true),
            ['class' => 'link-white']
        );?>

        </button>
    </div>
    <?php $message = Yii::$app->request->get('message');?>
    <p><?php print $message ?: '';?></p>
</div>
