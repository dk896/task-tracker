<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $modelComment app\models\tasks_comment\TasksComment */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    <div class="col-lg-9">
    <p>For comment fill out the following field:</p>
<div class="tasks_comment-form">

    <?php $form = ActiveForm::begin([
    'id' => 'comment-form',
    'fieldConfig' => [
        'template' => "<div>{label}</div><br><div class=\"col-lg-9\">{input}</div><br><div class=\"col-lg-9\">{error}</div><br>",
        'labelOptions' => ['class' => 'col-lg-2 control-label'],
    ],
]);?>

    <?=$form->field($model, 'body')->textarea(['autofocus' => true, 'rows' => 8]);?>

    <div class="form-group">
    <br><br>
        <?=Html::submitButton(
    'Submit',
    [
        'class' => 'btn btn-primary',
        'name' => 'comment-button',
    ]
);?>

    </div>

    <?php ActiveForm::end();?>

</div>
</div>
</div>
