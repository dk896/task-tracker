<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\tables\Task */

$this->title = 'Add comment to task';
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['task']];
$this->params['breadcrumbs'][] = ['label' => 'Task',
    'url' => ['view', 'id' => $model->task_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tasks-comment">

    <h1><?=Html::encode($this->title);?></h1>

    <?=$this->render('_comment_form',
    [
        'model' => $model,
    ]);?>

</div>
