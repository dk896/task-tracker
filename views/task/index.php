<?php
/** @var array $calendar */
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div>
<?=Yii::t('app/task', 'Current date: {0, date, dd MMMM yyyy}', time());?>
</div>
<table class="table table-bordered">
    <tr>
        <td>
        <?=Yii::t('app/task', 'Date of month');?>
        </td>
        <td><?=Yii::t('app/task', 'Event');?></td>
        <td><?=Yii::t('app/task', 'Done');?></td>
        <td><?=Yii::t('app/task', 'Total events');?></td>
    </tr>
    <?php foreach ($calendar as $day => $events) : ?>
        <tr>
            <td class="td-date"><span class="label label-success"><?=$day;?></span></td>
            <td>
                <?= (count($events) > 0) ?
                '<p>' . ucfirst($events[0]->name) . '</p><p class="small">' .
                ucfirst($events[0]->description) . '</p>'
                : '-';
                ?>
            </td>
            <td>
            <?=(count($events) > 0) ?
            '<p>' . Yii::$app->formatter->asBoolean($events[0]->done) : '-' . '</p>';
            ?>
            </td>
            <td class="td-event">
                <?= (count($events) > 0) ? Html::a(
                    count($events),
                    Url::to(['task/view', 'id' => $events[0]->id])
                ) : '-';
                ?>
            </td>
        </tr>
    <?php endforeach;?>
</table>
