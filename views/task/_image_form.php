<?php

/* @var $this yii\web\View */
/* @var $modelComment app\models\tasks_image\TasksImage */
/* @var $form yii\widgets\ActiveForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-lg-8">
        <p>For uploading image</p>
        <div class="tasks-image-form">

        <?php $form = ActiveForm::begin(
    [
        'id' => 'image-form',
        'fieldConfig' => [
            'template' => "{label}\n
            <div class=\"col-lg-3\">{input}</div>\n
            <div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]
);
?>

    <?=$form->field($modelImageFile, 'imageFile')->fileInput();?>

    <div class="form-group">

    <?=Html::submitButton(
    'Upload image',
    [
        'class' => 'btn btn-primary',
        'name' => 'image-button',
    ]
);
?>

    </div>

    <?php ActiveForm::end();?>

        </div>
    </div>
</div>
