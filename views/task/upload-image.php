<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin(
    [
        'options' => ['enctype' => 'multipart/form-data'],
    ]
);

echo $form
    ->field($model, 'imageFile')
    ->fileInput(['accept' => 'image/*']);

echo Html::submitButton(
    'Upload Image',
    [
        'class' => "btn btn-lg btn-primary",
        'name' => 'upload-button',
    ]
);

ActiveForm::end();
