<?php

return [
    'Date' => 'Дата',
    'Current date: {0, date, dd MMMM yyyy}' => 'Сегодня: {0, date, dd MMMM yyyy}',
    'Date of month' => 'День месяца',
    'Event' => 'Задача',
    'Done' => 'Выполнение',
    'Total events' => 'Всего задач',
    'Tasks' => 'Задачи',
    'Add comment' => 'Добавить комментарий',
    'Upload image' => 'Загрузить изображение',
];
