<?php

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\user\userSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?=Html::encode($this->title)?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php if (Yii::$app->user->can('createUser')): ?>
    <p>
        <?=Html::a(
    \Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']
)?>
    </p>
<?php endif;?>

    <?=GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'login',
            'first_name',
            'last_name',
            'email',
            'role_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]
);?>
</div>
