<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tables\user */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin();?>

    <?=$form->field($model, 'login')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'first_name')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'last_name')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'password')->passwordInput(['maxlength' => true])?>

    <?=$form->field($model, 'email')->textInput()?>

    <?=$form->field($model, 'role_id')->radioList([
	'1' => 'Admin',
	'2' => 'Boss',
	'3' => 'Project manager',
	'4' => 'Teamleader',
	'5' => 'User',
]);?>

    <div class="form-group">
        <?=Html::submitButton('Save', ['class' => 'btn btn-success'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
