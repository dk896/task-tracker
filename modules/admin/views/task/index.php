<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\tables\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Admin - Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">

    <h1><?=Html::encode($this->title);?></h1>
    <div class="row">
        <div class="col-md-6">
            <?php echo $this->render('_search', ['model' => $searchModel]);?>
        </div>
    </div>

    <p>
        <?=Html::a('Create Task', ['create'], ['class' => 'btn btn-success']);?>
    </p>

    <?=GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'name',
                // 'deadline:datetime',
                'deadline:date',
                'description:ntext',
                'user_id',
                [
                    'label' => 'Full name',
                    'content' => function ($model, $key, $index, $column) {
                        return $model->user->first_name . ' ' . $model->user->last_name;
                    },
                    'format' => ['text'],
                ],
                [
                    'attribute' => 'done',
                    'format' => ['boolean'],
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Actions',
                    'buttons' => [
                        'visibleButtons ' => [
                            'view' => \Yii::$app->user->can('view'),
                            'update' => \Yii::$app->user->can('update'),
                            'delete' => \Yii::$app->user->can('delete'),
                        ],
                    ]
                ],
            ],
        ]
    );?>
</div>
