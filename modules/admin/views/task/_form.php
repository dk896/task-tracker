<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\tables\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin();?>

    <?=$form->field($model, 'name')->textInput(['maxlength' => true]);?>

    <?=$form->field($model, 'deadline')->widget(
    DateTimePicker::className(),
    [
        'name' => 'Creation date and time',
        'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
        'options' => ['placeholder' => 'Select date and time'],
        'value' => \date('Y-m-d H:i:s'),
        'convertFormat' => false,
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd H:ii:ss',
            'startDate' => '2018-01-01 12:00:00',
            'todayHighlight' => true,
            'autoclose' => true,
        ],
    ]
);
?>

    <?=$form->field($model, 'description')->textarea(['rows' => 6]);?>

    <?=$form->field($model, 'user_id')->textInput();?>

    <?=$form->field($model, 'done')->radioList(['0' => 'Not done', '1' => 'Done']);?>

    <div class="form-group">
        <?=Html::submitButton('Save', ['class' => 'btn btn-success']);?>
    </div>

    <?php ActiveForm::end();?>

</div>
