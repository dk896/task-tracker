<?php

$db = require __DIR__ . '/db.php';

return [
    'request' => [
        // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
        'cookieValidationKey' => 'hWv9A15eGD82R_E9vodmYNCKCSNpms33',
    ],
    'cache' => [
        'class' => 'yii\redis\Cache',
    ],
    'user' => [
        'identityClass' => app\models\UserIdentity::class,
        'enableAutoLogin' => true,
        'loginUrl' => ['site/login'],
    ],
    'errorHandler' => [
        'errorAction' => 'site/error',
    ],
    'mailer' => [
        'class' => 'yii\swiftmailer\Mailer',
        // send all mails to a file by default. You have to set
        // 'useFileTransport' to false and configure a transport
        // for the mailer to send real emails.
        'useFileTransport' => true,
    ],
    'log' => [
        'traceLevel' => YII_DEBUG ? 3 : 0,
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
    'db' => $db,
    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
            'admin' => '/admin/task/index',
            'users' => '/admin/user/index',
            'user/<id:\d{1,3}>' => '/admin/user/view',
            'tasks' => '/task/index',
            'task/<id:\d{1,3}>' => '/task/view',
        ],
    ],
    'redis' => [
        'class' => 'yii\redis\Connection',
        'hostname' => 'localhost',
        'port' => 6379,
        'database' => 0,
    ],
    'i18n' => [
        'translations' => [
            'app*' => [
                'class' => \yii\i18n\PhpMessageSource::class,
                'basePath' => '@app/messages',
                'fileMap' => [
                    'app' => 'app.php',
                    'app/task' => 'task.php',
                    'app/error' => 'error.php',
                ],
            ],
        ],
    ],
    'authManager' => [
        'class' => 'yii\rbac\DbManager',
    ],
];
