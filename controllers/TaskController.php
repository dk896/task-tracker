<?php

namespace app\controllers;

use app\models\tasks_comment\TasksComment;
use app\models\tasks_image\TasksImage;
use app\models\task\Task;
use app\models\upload\UploadImageForm;
use app\models\user\User;
use Yii;
use yii\caching\DbDependency;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class TaskController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'view', 'upload-image', 'add-comment'],
                'rules' => [
                    [
                        'actions' => [
                            'index', 'view', 'upload-image', 'add-comment'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'boss', 'manager', 'tleader', 'user'],
                    ],
                    [
                        'actions' => [
                            'index', 'view', 'upload-image', 'add-comment'
                        ],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays calendar list of user`s tasks.
     *
     * @return string
     */
    public function actionIndex()
    {
        // Get current ID of logined user
        $userId = Yii::$app->user->getId() ?: '1000';
        $dependency = new DbDependency();
        $dependency->sql = 'SELECT MAX([[updated_at]]) FROM {{task}}';
        $duration = 3600 * 24 * 1;

        $cache = Yii::$app->cache;
        $key = 'calendar_' . $userId;

        if (!$calender = $cache->get($key)) {
            // Fill array keyes with [1, .., date("t")].
            // date("t") - count of days in current month
            $calendar = array_fill_keys(range(1, date("t")), []);

            foreach (Task::getByCurrentMonth($userId) as $task) {
                // Get current $task->date and create new DateTime object
                // $date->format("j") -- Day of the month: 1, 2, .., 31
                // Fill array $calender with $task objects
                $date = \DateTime::createFromFormat("Y-m-d H:i:s", $task->deadline);
                $calendar[$date->format("j")][] = $task;
            }

            $cache->set($key, $calendar, $duration, $dependency);
        } else {
            $calendar = $cache->get($key);
        }

        return $this->render('index', ['calendar' => $calendar]);
    }

    /**
     * Displays a single Task model.
     *
     * @param integer $id - ID
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $message = '';

        return $this->render(
            'view',
            [
                'model' => $this->findModel($id),
                'message' => $message,
            ]
        );
    }

    /**
     * Function for upload image
     *
     * @return boolean|string
     */
    public function actionUploadImage()
    {
        $modelImageFile = new UploadImageForm();
        $modelDBImage = new TasksImage();
        $modelDBImage->task_id = Yii::$app->request->get('task_id');

        if (Yii::$app->request->isPost) {
            $modelImageFile->imageFile
            = UploadedFile::getInstance($modelImageFile, 'imageFile');

            if ($modelImageFile->upload()) {
                $modelDBImage->name = $modelImageFile->newImageName;
                $modelDBImage->save();

                $message = 'Image successfully uploaded';

                return $this->redirect(
                    [
                        'view',
                        'id' => $modelDBImage->task_id,
                        'message' => $message,
                    ]
                );
            }
        }

        return $this->render('upload-image', ['model' => $modelImageFile]);
    }

    /**
     * Function for upload image
     *
     * @return boolean|string
     */
    public function actionAddComment()
    {
        $model = new TasksComment();
        $model->task_id = Yii::$app->request->get('task_id');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->task_id]);
        }

        return $this->render('add-comment', ['model' => $model]);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id - ID
     *
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
