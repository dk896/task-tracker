<?php

use yii\db\Migration;

/**
 * Class m180608_192552_add_column_done_to_task_table
 */
class m180608_192552_add_column_done_to_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'done', $this->tinyInteger()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task', 'done');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180608_192552_add_column_done_to_task_table cannot be reverted.\n";

        return false;
    }
    */
}
