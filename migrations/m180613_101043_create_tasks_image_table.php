<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks_image`.
 */
class m180613_101043_create_tasks_image_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tasks_image', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer(),
            'name' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'fk_task__tasks_image',
            'tasks_image',
            'task_id',
            'task',
            'id'
        );
        $this->createIndex('fk_task__tasks_image_idx', 'tasks_image', 'task_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('fk_task__tasks_image_idx', 'tasks_image');
        $this->dropForeignKey('fk_task__tasks_image', 'tasks_image');
        $this->dropTable('tasks_image');
    }
}
