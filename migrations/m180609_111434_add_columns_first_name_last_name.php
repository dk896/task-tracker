<?php

use yii\db\Migration;

/**
 * Class m180609_111434_add_columns_first_name_last_name
 */
class m180609_111434_add_columns_first_name_last_name extends Migration {
	/**
	 * {@inheritdoc}
	 */
	public function safeUp() {
		$this->addColumn('user', 'first_name', $this->string());
		$this->addColumn('user', 'last_name', $this->string());
	}

	/**
	 * {@inheritdoc}
	 */
	public function safeDown() {
		$this->dropColumn('user', 'last_name');
		$this->dropColumn('user', 'first_name');
	}

	/*
		    // Use up()/down() to run migration code without a transaction.
		    public function up()
		    {

		    }

		    public function down()
		    {
		        echo "m180609_111434_add_columns_first_name_last_name cannot be reverted.\n";

		        return false;
		    }
	*/
}
