<?php

use yii\db\Migration;

/**
 * Class m180606_051731_rename_column_date_to_deadline_in_task_table
 */
class m180606_051731_rename_column_date_to_deadline_in_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('task', 'date', 'deadline');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('task', 'deadline', 'date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180606_051731_rename_column_date_to_deadline_in_task_table cannot be reverted.\n";

        return false;
    }
    */
}
