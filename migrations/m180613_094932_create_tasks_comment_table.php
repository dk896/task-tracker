<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m180613_094932_create_tasks_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tasks_comment', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'fk_task__tasks_comment',
            'tasks_comment',
            'task_id',
            'task',
            'id'
        );
        $this->createIndex('fk_task__tasks_comment_idx', 'tasks_comment', 'task_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('fk_task__tasks_comment_idx', 'tasks_comment');
        $this->dropForeignKey('fk_task__tasks_comment', 'tasks_comment');
        $this->dropTable('tasks_comment');
    }
}
