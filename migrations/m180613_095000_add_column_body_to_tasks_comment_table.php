<?php

use yii\db\Migration;

/**
 * Class m180613_095000_add_column_body_to_tasks_comment_table
 */
class m180613_095000_add_column_body_to_tasks_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('tasks_comment', 'body', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('tasks_comment', 'body');
    }

    /*
// Use up()/down() to run migration code without a transaction.
public function up()
{

}

public function down()
{
echo "m180605_155602_add_column_to_task_table cannot be reverted.\n";

return false;
}
 */
}
