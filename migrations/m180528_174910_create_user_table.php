<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180528_174910_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string(50)->notNull(),
            'email' => $this->string(100)->notNull(),
            'password' => $this->string(100)->notNull(),
            'role_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->addForeignKey('fk_role_user', 'user', 'role_id', 'role', 'id');
        $this->createIndex('fk_role_user_idx', 'user', 'role_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('fk_role_user_idx', 'user');
        $this->dropForeignKey('fk_role_user', 'user');
        $this->dropTable('user');
    }
}
